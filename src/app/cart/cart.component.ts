import { Component, OnInit, inject } from '@angular/core';
import { FormArray, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Product } from '../product/product';
import { CartService } from './cart.service';
import { ProductsService } from '../product/products.service';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css',
})
export class CartComponent implements OnInit {
  private cartService = inject(CartService);
  private productService = inject(ProductsService);
  cartForm = new FormGroup({
    products: new FormArray<FormControl<number>>([]),
  });
  products: Product[] = [];

  private getProducts() {
    this.productService.getProducts().subscribe((products) => {
      this.cartService.cart?.products.forEach((item) => {
        const product = products.find((p) => p.id === item.productId);
        if (product) {
          this.products.push(product);
        }
      });
    });
  }

  private buildForm() {
    this.products.forEach(() => {
      this.cartForm.controls.products.push(
        new FormControl(1, { nonNullable: true }),
      );
    });
  }

  ngOnInit(): void {
      this.getProducts();
      this.buildForm();
  }
}
