import { inject } from '@angular/core';
import { CanActivateFn, CanMatchFn, Router } from '@angular/router';
import { AuthService } from './auth.service';

// ajout de CanMatchFn pour les routes en lazy loading
export const authGuard: CanActivateFn | CanMatchFn = () => {
  const authService = inject(AuthService);
  const router = inject(Router);
  if(authService.isLoggedIn()) {
    return true
  }
  return router.parseUrl('/'); //renvoie un objet UrlTree
};
