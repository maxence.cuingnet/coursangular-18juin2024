import { AfterViewInit, Component, Inject, Signal, computed, signal } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { ProductListComponent } from './product/product-list/product-list.component';
import { APP_SETTINGS, AppSettings } from './app.settings';
import { Observable } from 'rxjs';
import { AuthComponent } from './auth/auth.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, ProductListComponent, AuthComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements AfterViewInit {
  /* title!: Signal<string>; //Signal indique que le signal est calculé
  currentDate = signal(new Date());
  title$ = new Observable(observer => {
    setInterval(() => {
      observer.next();
    }, 1000)
  }) */
  title = 'Application';
  constructor(@Inject(APP_SETTINGS) public settings: AppSettings) {
   /*  this.title$.subscribe(this.setTitle)
    this.title = computed(() => {
      return `${this.settings.title} (${this.currentDate()})`;
    }) */
  }

  ngAfterViewInit(): void {
      this.title = 'Application de produits'
  }
  /* private setTitle = () => {
    const timestamp = new Date();
    this.currentDate.update(() => timestamp)
  } */
 }
