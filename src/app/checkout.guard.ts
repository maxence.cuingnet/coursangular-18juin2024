import { CanDeactivateFn } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CartService } from './cart/cart.service';
import { inject } from '@angular/core';

export const checkoutGuard: CanDeactivateFn<CartComponent> = () => {
  const cartService = inject(CartService);
  if (cartService.cart) {
    const confirmation = confirm(
      'Vous avez encore des produits dans le panier. Continuer ?',
    );
    return confirmation;
  }
  return true
};
