import { ApplicationConfig, provideZoneChangeDetection, ErrorHandler } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';

import { routes } from './app.routes';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { APP_SETTINGS, appSettings } from './app.settings';
import { authInterceptor } from './auth.interceptor';
import { ProductsService } from './product/products.service';
import { AppErrorHandler } from './app-error-handler';

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }),
    provideRouter(routes, withComponentInputBinding()),
    provideHttpClient(/* withInterceptors([authInterceptor]) */),
    { provide: APP_SETTINGS, useValue: appSettings },
    {
      provide: ProductsService, useClass: ProductsService
    }, 
    {
      provide: ErrorHandler, useClass: AppErrorHandler
    }
  ],
};
