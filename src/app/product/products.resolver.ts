import { ResolveFn } from '@angular/router';
import { ProductsService } from './products.service';
import { inject } from '@angular/core';
import { Product } from './product';

export const productsResolver: ResolveFn<Product[]> = (route, state) => {
  // routeur attend que l'observable ou la promesse soit résolu
  // si event asynchrone ne se finit pas, la navigation ne continue pas
  const productService = inject(ProductsService);
  const limit = Number(route.queryParamMap.get('limit'));
  return productService.getProducts(limit);
};
