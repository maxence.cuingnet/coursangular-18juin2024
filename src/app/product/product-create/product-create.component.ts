import { Component, EventEmitter, OnInit, Output, inject } from '@angular/core';
import { ProductsService } from '../products.service';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { maxPriceValidator } from './max-price.validator';

@Component({
  selector: 'app-product-create',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './product-create.component.html',
  styleUrl: './product-create.component.css',
})
export class ProductCreateComponent implements OnInit {
  constructor(
    private productsService: ProductsService,
    private router: Router,
    private builder: FormBuilder,
  ) {}

  productForm:
    | FormGroup<{
        title: FormControl<string>;
        price: FormControl<number | undefined>;
        category: FormControl<string>;
        extra: FormGroup<{
          image: FormControl<string>;
          description: FormControl<string>;
        }>;
      }>
    | undefined;

  private buildForm() {
    this.productForm = this.builder.nonNullable.group({
      title: ['', Validators.required, Validators.min(1)],
      price: this.builder.nonNullable.control<number | undefined>(undefined, {
        validators: [
          maxPriceValidator(100)
        ]
      }),
      category: [''],
      extra: this.builder.nonNullable.group({
        image: [''],
        description: [''],
      }),
    });
  }

  ngOnInit(): void {
      this.buildForm();
  }

  createProduct() {
    this.productsService
      .addProduct(this.productForm!.value)
      .subscribe(() => this.router.navigate(['/products']));
  }
}
