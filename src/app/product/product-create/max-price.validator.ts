import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function maxPriceValidator(price: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const isMax = control.value <= price;
        return isMax ? null : { priceMaximum: true }
    }
}