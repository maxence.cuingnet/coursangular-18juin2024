import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  computed,
  effect,
  inject,
  OnInit,
  Signal,
  signal,
} from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { FavoritesComponent } from '../favorites/favorites.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { Product } from '../product';
import { ProductViewComponent } from '../product-view/product-view.component';
import { toObservable, toSignal } from '@angular/core/rxjs-interop'; // convertir un observable en signal
import { ProductCreateComponent } from '../product-create/product-create.component';
import { ActivatedRoute, RouterLink, RouterOutlet } from '@angular/router';
import { Observable, of, switchMap } from 'rxjs';

@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [
    AsyncPipe,
    FavoritesComponent,
    ProductDetailComponent,
    ProductViewComponent,
    ProductCreateComponent,
    RouterLink,
    RouterOutlet
  ],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListComponent implements OnInit {
  selectedProduct: Product | undefined;
  products$!: Observable<Product[] | undefined>
  //products: Signal<Product[]>;
  route= inject(ActivatedRoute);

  ngOnInit(): void {
    // exploitation de la donnée fournit par le resolver dans le app.routes.ts
    this.products$ = this.route.data.pipe(
      switchMap(data => of(data['products']))
    )
  }


  onAdded(product: Product) {
    alert(`${product?.title} ajouté au panier`);
  }

  onRefresh() {
    // this.products$.subscribe();
  }

  /* remove(product: Product) {
    this.products$.deleteProduct(product.id).subscribe()
  } */
}
