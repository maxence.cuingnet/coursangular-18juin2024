import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  inject,
} from '@angular/core';
import { Product } from '../product';
import { AsyncPipe, CurrencyPipe } from '@angular/common';
import { Observable, switchMap, tap } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { FormsModule } from '@angular/forms';
import { CartService } from '../../cart/cart.service';

@Component({
  selector: 'app-product-view',
  standalone: true,
  imports: [CurrencyPipe, AsyncPipe, FormsModule],
  templateUrl: './product-view.component.html',
  styleUrl: './product-view.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductViewComponent implements OnInit, OnChanges {
  @Input() product: Product | undefined;
  @Input() id: string | undefined;
  @Output() added = new EventEmitter();
  price: number | undefined;
  product$: Observable<Product> | undefined;
  productService = inject(ProductsService);
  private router = inject(Router);
  private cartService = inject(CartService);

  addToCart(id: number) {
    this.cartService.addProduct(id).subscribe();
  }

  ngOnInit(): void {
    this.product$ = this.productService.getProduct(Number(this.id!));
  }

  ngOnChanges(): void {
    this.product$ = this.productService.getProduct(Number(this.id!));
  }

  changePrice(product: Product) {
    this.productService
      .updateProduct(product.id, this.price!)
      .subscribe(() => this.router.navigate(['/products']));
  }
}
