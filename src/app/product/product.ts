export interface Product {
  id: number;
  title: string;
  name?: string;
  category: string;
  price: number;
}
