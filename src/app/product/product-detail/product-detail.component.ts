import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, WritableSignal, inject, signal } from '@angular/core';
import { ProductsService } from '../products.service';
import { Observable, Subject, takeUntil } from 'rxjs';
import { Product } from '../product';
import { AsyncPipe } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  standalone: true,
  imports: [AsyncPipe, RouterLink],
  templateUrl: './product-detail.component.html',
  styleUrl: './product-detail.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  @Input() id: number | undefined;
  @Output() deleted = new EventEmitter();
  private productService = inject(ProductsService);
  product: WritableSignal<Product | undefined> = signal(undefined);
  private productSub = new Subject<void>();
  

  getProduct() {
    this.productService.getProduct(this.id!).pipe(
      takeUntil(this.productSub)
    ).subscribe(product => this.product.set(product))
  }

  ngOnInit(): void {
    this.getProduct();
  }

  ngOnDestroy(): void {
    this.productSub.next();
    this.productSub.complete();
  }

}
