import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
  HttpStatusCode,
} from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, of, retry, tap, throwError } from 'rxjs';
import { APP_SETTINGS } from '../app.settings';
import { Product } from './product';

@Injectable()
export class ProductsService {
  private http = inject(HttpClient);
  private productsUrl = inject(APP_SETTINGS).apiUrl + '/computerItems';
  private products: Product[] = [];

  getProducts(limit?: number): Observable<Product[]> {
    if (this.products.length === 0) {
      const options = new HttpParams().set('limit', limit || 10);
      return this.http
        .get<Product[]>(this.productsUrl, {
          params: options,
        })
        .pipe(
          map((products) => {
            this.products = products;
            return products;
          }),
          retry(2)
        );
    }
    return of(this.products);
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`${this.productsUrl}/${id}`);
  }

  addProduct(newProduct: Partial<Product>): Observable<Product> {
    return this.http.post<Product>(this.productsUrl, newProduct).pipe(
      map((product) => {
        return product;
      }),
    );
  }

  deleteProduct(id: number): Observable<void> {
    return this.http.delete<void>(`${this.productsUrl}/${id}`).pipe(
      tap(() => {
        this.products = this.products.filter((p) => p.id !== id);
      }),
    );
  }

  updateProduct(id: number, price: number): Observable<Product> {
    return this.http
      .patch<Product>(`${this.productsUrl}/${id}`, { price })
      .pipe(
        map((product) => {
          const index = this.products.findIndex((p) => p.id === id);
          this.products[index].price = price;
          return product;
        }),
      );
  }

  
}
