import { Routes } from '@angular/router';
import { ProductListComponent } from './product/product-list/product-list.component';
import { CartComponent } from './cart/cart.component';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { ProductViewComponent } from './product/product-view/product-view.component';
import { authGuard } from './auth.guard';
import { checkoutGuard } from './checkout.guard';
import { productsResolver } from './product/products.resolver';

export const routes: Routes = [
  {
    path: 'products',
    component: ProductListComponent,
    children: [
      { path: 'new', component: ProductCreateComponent },
      { path: ':id', component: ProductViewComponent },
    ],
    resolve: {
      products: productsResolver,
    },
    //providers: [ProductsService],
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [authGuard],
    canDeactivate: [checkoutGuard],
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.routes'),
    canMatch: [authGuard],
  },
  { path: '', redirectTo: 'products', pathMatch: 'full' },
  { path: '**', redirectTo: 'products' },
];
