import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppComponent],
    }).compileComponents();
  });
  

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have the 'application' title`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('application');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges(); // lance ngOnInit en 1ier et ensuite ngOnChanges
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toContain('Hello, application');
  });

  it('devrait changer le titre', async () => {
    const titleSpy = jasmine.createSpyObj('Title', ['getTitle', 'setTitle'])
    titleSpy.getTitle.and.returnValue('Mon titre')
    await TestBed.configureTestingModule({
      import: [SpyComponent],
      providers: [
        { provide: Title, useValue: titleSpy}
      ]
    }).compileComponents()
    const title = TestBed.inject(Title); //instance du service Title
    const spy = spyOn(title, 'setTitle') // appelle la méthode du service Title
    component.ngOnInit()
    expect(spy).toHaveBeenCalledWith('application')
  })
});
