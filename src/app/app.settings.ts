import { InjectionToken } from "@angular/core";

export interface AppSettings {
  title: string;
  version: string;
  apiUrl: string;
  authUrl: string;
}

export const appSettings: AppSettings = {
  title: 'E-shop',
  version: '1.0',
  apiUrl: 'http://localhost:3000',
  authUrl : 'https://fakestoreapi.com/auth/login'
}

export const APP_SETTINGS = new InjectionToken<AppSettings>('appSettings');