import { Injectable, computed, inject, signal } from '@angular/core';
import { APP_SETTINGS } from './app.settings';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private accessToken = signal('');
  private authUrl = inject(APP_SETTINGS).authUrl;
  private http = inject(HttpClient);
  isLoggedIn = computed(() => this.accessToken() !== '');

  login(username: string, password: string): Observable<string> {
    return this.http
      .post<string>(this.authUrl, {
        username,
        password,
      })
      .pipe(tap((token) => this.accessToken.update(() => token)));
  }

  logout() {
    this.accessToken.update(() => '');
  }
}
