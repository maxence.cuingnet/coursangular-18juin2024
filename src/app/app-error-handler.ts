import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  handleError(error: any) {
    const err = error.rejection || error; // rejection ==> erreur gérée par Zone.js
    let message = '';
    if (err instanceof HttpErrorResponse) {
      switch (error.status) {
        case 0: // erreur côté client
          message = 'Erreur côté client';
          break;
        case HttpStatusCode.InternalServerError:
          message = 'Erreur de serveur';
          break;
        case HttpStatusCode.BadRequest:
          message = 'Erreur de requête';
          break;
        default:
          message = 'Erreur inconnue';
      }
    } else {
        message = 'Application Error'
    }
    console.error(message, err)
  }
}
